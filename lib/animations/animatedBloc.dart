import 'package:flutter/material.dart';


class AnimatedBlock extends StatefulWidget {
  const AnimatedBlock({ Key key }) : super (key: key);

  @override
  _AnimatedBlockState createState() => _AnimatedBlockState();
}

class _AnimatedBlockState extends State<AnimatedBlock> with SingleTickerProviderStateMixin<AnimatedBlock> {
  AnimationController _controller;
  double blocWidth = 200;
  //j'ai multiplié le controller par cette valeur pour faire l'animation vite fait  au final mais merci je comprend le fonctionnement maintenant ! 

  @override
  void initState() {
    super.initState();
    _controller = new AnimationController( vsync: this, duration: const Duration(seconds: 5));
    _controller.forward();
  }

  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
          children: [
              AnimatedBuilder (
                animation: _controller,
                builder: (_, __) {
                  return Container(
                    width: _controller.value * blocWidth,
                    height: 20, 
                    color: Colors.red
                  );
                }
              ),
            FloatingActionButton(
              onPressed: () => setState(() => blocWidth += 20),
              tooltip: 'Increment Counter',
              child: const Icon(Icons.add),
            ),
          ]
    );
  }
}