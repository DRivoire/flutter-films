import 'package:flutter/material.dart';
import 'details.dart';
//repositories
import 'repositories/getSeries.dart';
import 'repositories/getMovies.dart';
import 'repositories/getBadMovies.dart';
//animations
import 'animations/animatedBloc.dart';

//Models
import 'models/movie.dart';



class FilmArchive extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Scaffold (
      appBar: AppBar(
        toolbarHeight: 0,
      ),
      body:
      Center(
        child: Stack(
          fit: StackFit.expand,
          children: [
            Container(
              color: Colors.black,
              child: ListView(
                scrollDirection: Axis.vertical,
                children:[
                  Center(child: AnimatedBlock()),
                  Container(
                    padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                    margin: EdgeInsets.fromLTRB(0, 25, 0, 0),
                    child: Text(
                      'Popular Movies',
                      style: TextStyle(color: Colors.white, fontSize: 30, fontWeight: FontWeight.bold)
                    ),
                  ),
                  FutureBuilder(
                    future: getMovies(),
                    builder: (context, snapshot) {
                      if ( snapshot.connectionState == ConnectionState.done) {
                        return Container(
                          margin: EdgeInsets.fromLTRB(0, 15, 0, 0),
                          height: 200,
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: snapshot.data.length,
                            itemBuilder: (context, index) {
                              Movie movie = snapshot.data[index];
                              return GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => Details(id: movie.id)),
                                  );
                                },
                                child: Container(
                                  margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(5),
                                    child: Image.network( 'https://image.tmdb.org/t/p/w500/' + movie.url)
                                    )
                                  )
                                );
                            },
                          ),
                        );
                      } else {
                        return Center(
                          child: CircularProgressIndicator()
                        );
                      }
                  }),
                  Container(
                    padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                    margin: EdgeInsets.fromLTRB(0, 25, 0, 0),
                    child: Text(
                      'Best TV Shows',
                      style: TextStyle(color: Colors.white, fontSize: 30, fontWeight: FontWeight.bold)
                    ),
                  ),
                  FutureBuilder(
                    future: getSeries(),
                    builder: (context, snapshot) {
                      if ( snapshot.connectionState == ConnectionState.done) {
                        return Container(
                          margin: EdgeInsets.fromLTRB(0, 15, 0, 0),
                          height: 200,
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: snapshot.data.length,
                            itemBuilder: (context, index) {
                              Movie movie = snapshot.data[index];
                              return GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => Details(id: movie.id)),
                                  );
                                },
                                child: Container(
                                  margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(5),
                                    child: Image.network( 'https://image.tmdb.org/t/p/w500/' + movie.url)
                                    )
                                  )
                                );
                            },
                          ),
                        );
                      } else {
                        return Center(
                          child: CircularProgressIndicator()
                        );
                      }
                  }),
                  Container(
                    padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                    margin: EdgeInsets.fromLTRB(0, 25, 0, 0),
                    child: Text(
                      'Best pranks',
                      style: TextStyle(color: Colors.white, fontSize: 30, fontWeight: FontWeight.bold)
                    ),
                  ),
                  FutureBuilder(
                    future: getBadMovies(),
                    builder: (context, snapshot) {
                      if ( snapshot.connectionState == ConnectionState.done) {
                        return Container(
                          margin: EdgeInsets.fromLTRB(0, 15, 0, 0),
                          height: 200,
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: snapshot.data.length,
                            itemBuilder: (context, index) {
                              Movie movie = snapshot.data[index];
                              return GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => Details(id: movie.id)),
                                  );
                                },
                                child: Container(
                                  margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(5),
                                    child: Image.network( 'https://image.tmdb.org/t/p/w500/' + movie.url)
                                    )
                                  )
                                );
                            },
                          ),
                        );
                      } else {
                        return Center(
                          child: CircularProgressIndicator()
                        );
                      }
                  }),
                ],
              )
            )
          ]
        )
      ),
    );
  }
}
