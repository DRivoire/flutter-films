class Movie {
  final String title;
  final String url;
  final String summary;
  final int id;


  Movie({ this.title, this.url, this.summary, this.id});

  factory Movie.fromJson(Map<String, dynamic> json) {
    return Movie(
      title: json['title'],
      url:json['poster_path'],
      summary:json['overview'],
      id:json['id']
    );
  }
}

class SoloMovie {
  final String title;
  final String url;
  final String summary;
  final List tags;
  final bool adult;


  SoloMovie({ this.title, this.url, this.summary, this.tags, this.adult});

  factory SoloMovie.fromJson(Map<String, dynamic> json) {
    return SoloMovie(
      title: json['title'],
      url:json['poster_path'],
      summary:json['overview'],
      tags:json['genres'],
      adult:json['adult'],
    );
  }
}