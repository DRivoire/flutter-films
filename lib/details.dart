import 'package:flutter/material.dart';
import 'filmArchive.dart';
import 'repositories/getMovie.dart';
import 'models/movie.dart';

class Details extends StatelessWidget {
  Details({Key key, @required this.id}) : super(key: key);

  final int id;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 0,
      ),
      body: Center(
        child: FutureBuilder (
          future: getSoloMovie(id),
          builder: (context, snapshot) {
            SoloMovie item = snapshot.data;
            if ( snapshot.connectionState == ConnectionState.done) {
              return Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  Image.network(
                    'https://image.tmdb.org/t/p/w500/' + item.url ,
                    fit: BoxFit.cover
                  ),
                  Positioned(
                    top: 0,
                    child: Container(
                      height: 50,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Color.fromRGBO(0, 0, 0, .5),
                            blurRadius: 2.0,
                            spreadRadius: 2.0,
                          )
                        ]
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          IconButton(
                            icon: Icon(Icons.arrow_back),
                            color: Colors.white,
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => FilmArchive()),
                              );
                            },
                          ),
                        ]
                      )
                    ),
                  ),
                  Positioned(
                    bottom: 0,
                    child:
                        Container(
                          height: MediaQuery.of(context).size.height*0.6,
                          width: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.fromLTRB(20, 70, 20, 20),
                          decoration: BoxDecoration(
                            gradient:(
                              LinearGradient(
                                begin: Alignment.bottomCenter,
                                end: Alignment.topCenter,
                                stops: [
                                  0.7,
                                  1
                                ],
                                colors:[ Colors.black, Colors.transparent ]
                            ))
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                item.title,
                                style: TextStyle( color: Colors.white, fontSize: 30, fontWeight: FontWeight.bold,)
                              ),
                              Container(
                              margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                              child : Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [ 
                                        TextSpan(text: item.adult == true ? "18+ -  2020  -  " : "12+ -  2020  -  " , style: TextStyle( color: Colors.white, fontSize: 15, fontWeight: FontWeight.bold,)),
                                        TextSpan(text: '7.6', style: TextStyle(color: Colors.yellow, fontSize: 15, fontWeight: FontWeight.bold,)),                                  
                                      ]
                                    )
                                  ),
                                ],)
                              ),
                              Container(
                                height: 35,
                                margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                child: ListView.builder(
                                      scrollDirection: Axis.horizontal,
                                      itemCount: item.tags.length,
                                      itemBuilder: (context, index) {
                                        return Container(
                                              padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
                                              margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                                              decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(5),
                                                color: Colors.white,
                                              ),
                                              child: Text(item.tags[index]['name'], style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold))
                                            );
                                          }
                                )
                              ),
                              Container(
                                margin: EdgeInsets.fromLTRB(0, 30, 0, 0),
                                child: Text.rich(
                                  TextSpan(
                                    children: [ 
                                      TextSpan(text: 'Cast: ', style: TextStyle( color: Colors.white, fontSize: 15, fontWeight: FontWeight.bold,)),
                                      TextSpan(text: 'Millie Boby Brown  Henri Cavill  Sam Clafin  Helena Bonham Carter', style: TextStyle(color: Colors.white, fontSize: 15, fontWeight: FontWeight.normal,)),                                  
                                    ]
                                  )
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                                child:
                                  Text(
                                    'Summary',
                                    style: TextStyle(color: Colors.white, fontSize: 20, fontWeight: FontWeight.bold,),                                  
                                  )
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width,
                                height: MediaQuery.of(context).size.height *.12,
                                margin: EdgeInsets.only(top: 10),
                                child: ListView(
                                  children: <Widget>[
                                    Text(
                                      item.summary,
                                      style: TextStyle(color: Colors.white, fontSize: 15, fontWeight: FontWeight.normal,),
                                    )
                                ]),
                              ),
                            ],
                          )
                        )
                  )
                ],
              );
            } else {
              return Center(
                child: CircularProgressIndicator()
              );
            }
          }
        ),
      ),
    );
  }
}