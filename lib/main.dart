import 'package:flutter/material.dart';
import 'login.dart';
import 'filmArchive.dart';
import 'package:firebase_core/firebase_core.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.red,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: FutureBuilder(
        future: Firebase.initializeApp(),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Center(child: Text(snapshot.error),);
          }

          // Once complete, show your application
          if (snapshot.connectionState == ConnectionState.done) {
            return FilmArchive();
          }
          
          return Text('loading');
        })
    );
  }
}
