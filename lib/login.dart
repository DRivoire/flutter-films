import 'package:flutter/material.dart';
import 'package:email_validator/email_validator.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'filmArchive.dart';

class Login extends StatefulWidget {
  const Login({ Key key }) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final _formKey = GlobalKey<FormState>();
  String email;
  String password;

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Center(
        child: Stack(
          fit: StackFit.expand,
          children: <Widget> [
            Image.asset(
              "assets/images/enola.jpg",
              fit: BoxFit.cover
            ),
            Positioned(
              bottom: 0,
              child: Container(
                height: MediaQuery.of(context).size.height*0.6,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  gradient:(
                    LinearGradient(
                      begin: Alignment.bottomCenter,
                      end: Alignment.topCenter,
                      stops: [
                        0.70,
                        1
                      ],
                      colors:[ Colors.black, Colors.transparent ]
                  ))
                ),
                padding: EdgeInsets.fromLTRB(20, 50, 20, 10),
                child: 
                  Form(
                    key: _formKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget> [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget> [
                            Text(
                              'Login',
                              style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold, color: Colors.white)
                            )
                          ]
                        ),
                        TextFormField(
                          onChanged: (value)=> {
                            email = value
                          },
                          decoration: InputDecoration(
                            focusColor: Colors.white,
                            hoverColor: Colors.white,
                            hintText: 'Complete please',
                            contentPadding: const EdgeInsets.all(20.0),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.white30,
                              ),
                              borderRadius: BorderRadius.circular(0),
                            ),
                            disabledBorder: InputBorder.none,
                            errorBorder: InputBorder.none,
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.white30,
                              ),
                              borderRadius: BorderRadius.circular(0),
                            ),
                            focusedErrorBorder: InputBorder.none,
                            filled: true,
                            fillColor: Colors.white24,
                          ),
                          validator:  (email) {
                            if (EmailValidator.validate( email ) == false) {
                              return 'You must enter a valid email please';
                            } else {
                              return null;
                            }
                          },
                          // (value) => EmailValidator.validate(value) ? null : "Please enter a valid email",
                        ),
                        TextFormField(
                          onChanged: (value)=> {
                            password = value
                          },
                          decoration: InputDecoration(
                            focusColor: Colors.white,
                            hoverColor: Colors.white,
                            hintText: 'Complete please',
                            contentPadding: const EdgeInsets.all(20.0),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.white30,
                              ),
                              borderRadius: BorderRadius.circular(0),
                            ),
                            disabledBorder: InputBorder.none,
                            errorBorder: InputBorder.none,
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.white30,
                              ),
                              borderRadius: BorderRadius.circular(0),
                            ),
                            focusedErrorBorder: InputBorder.none,
                            filled: true,
                            fillColor: Colors.white24,
                          ),
                          validator: (password) {
                            if (password.length < 8) {
                              return 'too short';
                            }
                            return null;
                          },
                        ),
                        Container(
                          child: ElevatedButton(
                            onPressed: () {
                              if (_formKey.currentState.validate()) {
                                checkAuth(context, email, password);
                              }
                            },
                            child: Text('Submit'),
                          ),
                        )
                      ]
                    )
                  )
              )
            )
          ]
        )
      )      
    );
  }

}


Future checkAuth(context, email, password) async{
  print(email);
  print(password);

  try {
    UserCredential userCredential = await FirebaseAuth.instance.signInWithEmailAndPassword(
      email: email,
      password: password
    );

    Navigator.push(
      context,
      MaterialPageRoute( builder: (context) => FilmArchive()),
    );
    print(userCredential);
  } on FirebaseAuthException catch (e) {
    if (e.code == 'user-not-found') {
      print('non existant');
      createUser(context, email, password);

    } else if (e.code == 'wrong-password') {
      print('mauvais mot de pass');
    }
  }
}

Future createUser(context, email, password) async{
  try {
    UserCredential userCredential = await FirebaseAuth.instance.createUserWithEmailAndPassword(
      email: email,
      password: password
    );

    Navigator.push(
      context,
      MaterialPageRoute( builder: (context) => FilmArchive()),
    );
    print(userCredential);
  } on FirebaseAuthException catch (e) {
    if (e.code == 'weak-password') {
      print('The password provided is too weak.');
    } else if (e.code == 'email-already-in-use') {
      print('The account already exists for that email.');
    }
  } catch (e) {
    print(e);
  }
}