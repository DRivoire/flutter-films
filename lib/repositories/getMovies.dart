import 'package:http/http.dart' as http;
import 'dart:convert';
import '../models/movie.dart';

var movieURI = 'https://api.themoviedb.org/3/discover/movie?api_key=62feaff3d2cf094a340f530fbf25bde9&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1';

getMovies() async{
    var response = await http.get(movieURI);

    if (response.statusCode == 200) {
        var jsonResponse = json.decode(response.body);
        var results = jsonResponse['results'];
        var movieList = [];

        results.forEach((el)=> movieList.add( Movie.fromJson(el)));

        return movieList;
      } else {
        print('merde');
      }
}