import 'package:http/http.dart' as http;
import 'dart:convert';
import '../models/movie.dart';
// api key : 62feaff3d2cf094a340f530fbf25bde9
var seriesURL = 'https://api.themoviedb.org/3/discover/movie?api_key=62feaff3d2cf094a340f530fbf25bde9&language=en-US&sort_by=release_date.asc&include_adult=false&include_video=false&page=1';

getSeries() async{
    var response = await http.get(seriesURL);
    if (response.statusCode == 200) {
        var jsonResponse = json.decode(response.body);
        var results = jsonResponse['results'];
        var seriesList = [];

        results.forEach((el)=> seriesList.add( Movie.fromJson(el)));

        return seriesList;
      } else {
        print('damn it');
      }
}