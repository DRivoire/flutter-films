import 'package:http/http.dart' as http;
import 'dart:convert';
import '../models/movie.dart';
// api key : 62feaff3d2cf094a340f530fbf25bde9
var badMoviesURL = 'https://api.themoviedb.org/3/discover/movie?api_key=62feaff3d2cf094a340f530fbf25bde9&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1';

getBadMovies() async{
    var response = await http.get(badMoviesURL);
    if (response.statusCode == 200) {
        var jsonResponse = json.decode(response.body);
        var results = jsonResponse['results'];
        var badMoviesList = [];

        results.forEach((el)=> {
          badMoviesList.add( Movie.fromJson(el))
        });

        return badMoviesList;
      } else {
        print('damn it');
      }
}