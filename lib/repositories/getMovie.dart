import 'package:http/http.dart' as http;
import 'dart:convert';
import '../models/movie.dart';


Future<Movie> getMovie(int id) async{
    var itemURI = "https://api.themoviedb.org/3/movie/$id?api_key=62feaff3d2cf094a340f530fbf25bde9&language=en-US";

    var response = await http.get(itemURI);

    if (response.statusCode == 200) {
        var result = json.decode(response.body);

        var item = Movie.fromJson(result);

        return item;
      } else {
        print('getMovie error');
        return null;
      }
}

Future<SoloMovie> getSoloMovie(int id) async{
    var itemURI = "https://api.themoviedb.org/3/movie/$id?api_key=62feaff3d2cf094a340f530fbf25bde9&language=en-US";

    var response = await http.get(itemURI);

    if (response.statusCode == 200) {
        var result = json.decode(response.body);

        var item = SoloMovie.fromJson(result);

        return item;
      } else {
        print('getSoloMovieError');
        return null;
      }
}